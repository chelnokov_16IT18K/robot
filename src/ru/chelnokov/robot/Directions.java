package ru.chelnokov.robot;

public enum Directions {
    UP,
    DOWN,
    LEFT,
    RIGHT
}
